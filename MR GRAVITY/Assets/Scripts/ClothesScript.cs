﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClothesScript : MonoBehaviour
{

    public GameObject buy;
    public GameObject pick;
    public GameObject picked;
    public int index;
    public int price;


    void Start()
    {
        if (PlayerPrefs.GetInt(index + "c", 0) == 0)
        {
            buy.SetActive(true);
            pick.SetActive(false);
            picked.SetActive(false);
        }
        else
        {
            if (PlayerPrefs.GetInt("c", 0) == index)
            {
                buy.SetActive(false);
                pick.SetActive(false);
                picked.SetActive(true);
                Shop.cs = this;
            }
            else
            {
                buy.SetActive(false);
                pick.SetActive(true);
                picked.SetActive(false);
            }
        }
    }

    public void pickButton()
    {
        PlayerPrefs.SetInt("c", index);
        buy.SetActive(false);
        pick.SetActive(false);
        picked.SetActive(true);

        Shop.cs.change();
        Shop.cs = this;
    }

    public void BuyButton()
    {
        if (Shop.gems >= price)
        {
            PlayerPrefs.SetInt(index + "c", 1);
            Shop.gems += -price;
            Shop.changeText = true;

            pickButton();
        }
    }

    public void change()
    {
        buy.SetActive(false);
        pick.SetActive(true);
        picked.SetActive(false);
    }

}

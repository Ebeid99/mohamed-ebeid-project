﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

	bool a;
	public Transform rot1;
	public Transform rot2;
	public static float speed;
	bool canJump;
    public GameObject ps;
    public AudioSource ding;
    public AudioSource swoosh;
    public static bool canMove;
    public GameObject[] clothes;

    public Collider col;

    private void Awake()
    {
        int i = PlayerPrefs.GetInt("c", 0);
        clothes[i].SetActive(true);

    }

    void Start () 
	{
		canJump = false;
		a = false;
        ding.enabled = false;
        canMove = false;
	}
	

	void Update () 
	{
        if (canMove)
        {
            if (Input.GetMouseButtonDown(0) && canJump)
            {
                swoosh.enabled = false;
                swoosh.enabled = true;
                if (a)
                {
                    Physics.gravity = new Vector3(0, -14.81F, 0);
                    a = false;
                    transform.rotation = rot1.rotation;
                }
                else
                {
                    Physics.gravity = new Vector3(0, 14.81F, 0);
                    a = true;
                    transform.rotation = rot2.rotation;
                }
            }

            transform.position += new Vector3(1, 0, 0) * Time.deltaTime * speed;
        }

	}

	void OnCollisionEnter(Collision collision)
	{
		canJump = true;
	}

	void OnCollisionExit(Collision other)
	{
		canJump = false;
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Gem")
        {
            ding.enabled = false;
            ding.enabled = true;
            ps.SetActive(false);
            ps.transform.position = other.transform.position;
            ps.SetActive(true);
            Destroy(other.gameObject);
            GameManagerScript.score++;
        }
        else if (other.tag == "Obstacle")
            GameManagerScript.die = true;
        else if (other.tag == "Finish")
            GameManagerScript.won = true;
        else if (other.tag == "Spawn")
            GameManagerScript.spawn();
        else
        {
            if (col == null)
            {
                col = other;
            }
            else
            {
                if (col != other)
                {
                    col.transform.parent.gameObject.SetActive(false);
                    col = other;
                }
            }
        }
    }

}

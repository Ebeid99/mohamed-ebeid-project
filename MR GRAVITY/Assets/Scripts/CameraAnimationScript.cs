﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraAnimationScript : MonoBehaviour
{

    public bool ready;
    public Camera cam1;
    public Camera cam2;

    void Start()
    {
        ready = false;
    }

    void Update()
    {
        if(ready)
        {
            cam1.enabled = true;
            cam2.enabled = false;
            this.enabled = false;
            PlayerScript.canMove = true;
        }
    }
}

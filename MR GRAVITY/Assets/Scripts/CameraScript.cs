﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {

	public Transform player;
	public Vector3 pos;
	float x;
	public float speed;

	void FixedUpdate () 
	{
		x = player.position.x;

		Vector3 pos2 = new Vector3(x, 2.1f, -8.61f) + pos;

		transform.position = Vector3.Lerp (transform.position, pos2, speed);

	}
}

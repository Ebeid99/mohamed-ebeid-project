﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Shop : MonoBehaviour
{
    public static int gems;
    public Text gemsText;
    public static bool changeText;

    public static ClothesScript cs;

    private void Awake()
    {
        gems = PlayerPrefs.GetInt("gems", 0);
        gemsText.text = gems.ToString();
        changeText = false;

    }

    private void Update()
    {
        if (changeText)
        {
            gemsText.text = gems.ToString();
            changeText = false;
        }
  
    }


}

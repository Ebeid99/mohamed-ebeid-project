﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManagerScript : MonoBehaviour
{

    public static int score;
    public Text scoreText;
    public GameObject[] ground;
    public static GameObject[] ground2;
    static int r;
    public static float d;
    public static int level;
    static int a;
    public GameObject pausePanel;
    public GameObject winPanel;
    public static bool won;
    public static bool die;
    public AudioSource gameOver;
    float t;
    public GameObject player;
    public Text hs;
    public Text sText;
    bool ounce;

    void Start()
    {
        ounce = false;
        t = 2;
        die = false;
        won = false;
        Physics.gravity = new Vector3(0, -14.81F, 0);
        Time.timeScale = 1;
        score = 0;
        ground2 = ground;
        d = 65f;
        level = int.Parse(SceneManager.GetActiveScene().name);
        PlayerScript.speed = level * 2;
        a = (level * 2) + 2;
    }

 
    void Update()
    {
        scoreText.text = score.ToString();
        if (won)
            winGame();
        else if (die)
        {
            if (t == 2)
            {
                player.SetActive(false);
                gameOver.enabled = false;
                gameOver.enabled = true;
            }
            t += -Time.deltaTime;
            if(t<=0)
                SceneManager.LoadScene(level - 1);
        }

    }


    public static void spawn()
    {
        if (a < 1)
            Instantiate(ground2[ground2.Length - 1], new Vector3(d, 0, 0), Quaternion.identity);
        else
        {
            r = Random.Range(0, ground2.Length - 1);
            Instantiate(ground2[r], new Vector3(d, 0, 0), Quaternion.identity);
            d += 44.5f;
            a--;
        }
    }

    public void pauseGame()
    {
        Time.timeScale = 0;
        pausePanel.SetActive(true);
    }

    public void resumeGame()
    {
        Time.timeScale = 1;
        pausePanel.SetActive(false);
    }

    void winGame()
    {
        if (!ounce)
        {
            if (score > PlayerPrefs.GetInt("hs", 0))
            {
                PlayerPrefs.SetInt("hs", score);
            }
            sText.text = "Score: " + score;
            hs.text = PlayerPrefs.GetInt("hs").ToString();
            int gems = PlayerPrefs.GetInt("gems", 0);
            gems += score;
            PlayerPrefs.SetInt("gems", gems);
            Time.timeScale = 0;
            winPanel.SetActive(true);
            ounce = true;
        }
    }

    public void mainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void nextLevel()
    {
        SceneManager.LoadScene(level);
    }

}

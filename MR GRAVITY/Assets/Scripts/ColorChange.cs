﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour
{

    float duration;
    private float t;
    private float t2;
    bool isReset;
    public Color color1;
    public Color color2;

    void Start()
    {
        duration = 9.5f;
        t = 0;
        t2 = 0;
        isReset = false;
    }

    void Update()
    {

        if (!isReset)
        {
            GetComponent<Renderer>().material.color = Color.Lerp(color1, color2, t);
            t += Time.deltaTime / duration;
            t2 = 0;
            if (t > 1)
                isReset = true;
        }
        else if (isReset)
        {
            GetComponent<Renderer>().material.color = Color.Lerp(color2, color1, t2);
            t2 += Time.deltaTime / duration;
            t = 0;
            if (t2 > 1)
                isReset = false;
        }

    }
}

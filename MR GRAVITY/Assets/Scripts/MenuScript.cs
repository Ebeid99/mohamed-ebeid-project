﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{

    string n;
    int a;
    public GameObject newPanel;
    public GameObject menuPanel;
    public Text inputNameText;
    public Text nameText;
    int gems;
    public Text gemsText;

    private void Awake()
    {
        gems = PlayerPrefs.GetInt("gems", 0);
        gemsText.text = gems.ToString();
        a = PlayerPrefs.GetInt("new", 0);
        if (a == 0)
        {
            newPanel.SetActive(true);
            menuPanel.SetActive(false);
        }
        else
        {
            n = PlayerPrefs.GetString("name");
            showMenu();
        }
    }

    void showMenu()
    { 
        newPanel.SetActive(false);
        menuPanel.SetActive(true);
        nameText.text = "Welcome " + n;
    }

    public void playButton()
    {
        n = inputNameText.text;
        if (n != "")
        {
            PlayerPrefs.SetString("name", n);
            PlayerPrefs.SetInt("new", 1);
            showMenu();
        }

    }


}
